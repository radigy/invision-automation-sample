1. Install Python from https://www.python.org/downloads/
2. Add to Path
3. Open console and type 'python -version' to verify
4. Open console and type 'pip install' 
5. Open console and type 'pip --version'  to verify
5. Open console and type 'pip install robotframework' 
6. Open console and type 'pip install robotframework-seleniumlibrary'
7. Open console and type 'pip list' to verify
8. Install IDE (for example ATOM or PyCharm)
9. Open IDE and add in project interpreters packages - robotframework, robotframework-seleniumlibrary, selenium
10. Install chrome webdriver from https://chromedriver.chromium.org/downloads and put it somewhere that your test can access 
(for example a bin folder relative to your tests).Now you need to set the environment variable to point the chrome driver.
11. Download repository 
12. Open console, navigate to your local repository and type 
'robot -d ../../results --variable COMMON_BROWSER:Chrome 01__login_TC.robot'
to run login test cases in chrome 

Full instalation documentation - https://github.com/robotframework/robotframework/blob/master/INSTALL.rst 
