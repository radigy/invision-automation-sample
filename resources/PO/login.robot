*** Settings ***
Library  SeleniumLibrary

Resource  ${EXECDIR}${/}..${/}..${/}resources${/}common.robot
Resource  ${EXECDIR}${/}..${/}..${/}resources${/}variables.robot


*** Keywords ***
User Navigate To Login Page
    [Documentation]  Go to login page and verify youre in correct section
    Wait Until Page Contains Element  ${HOME_PAGE_LOGIN_LINK}
    Click Link  ${HOME_PAGE_LOGIN_LINK}
    Wait Until Page Contains Element  signin_email
    Title Should Be  ${LOGIN_PAGE_TITLE}

Fill Email And Password With Invalid Credentials
    [Documentation]  Fill Email And Password With Invalid Credentials
    [Arguments]  ${email}  ${password}
    Wait Until Page Contains Element  ${LOGIN_MAIL_INPUT}
    Input Password  ${LOGIN_MAIL_INPUT}  ${email}
    Input Password  ${LOGIN_PASSWORD_INPUT}  ${email}

Click Sign In Button
    [Documentation]  Click Sign In Button
    Wait Until Element Is Enabled  ${LOGIN_SING_IN_BUTTON}
    Click Element  ${LOGIN_SING_IN_BUTTON}

Login Validation Error Message Is Displayed
    [Documentation]  Login Validation Error Message Is Displayed
    Wait Until Page Contains  ${LOGIN_VALIDATION_ERROR_MESSAGE}
    Page Should Contain  ${LOGIN_VALIDATION_ERROR_MESSAGE}

Click Get Started Button
    [Documentation]  Click Get Started Button On Login Page
    Wait Until Page Contains Element  ${LOGIN_GET_STARTED_BUTTON}
    Click Element  ${LOGIN_GET_STARTED_BUTTON}
