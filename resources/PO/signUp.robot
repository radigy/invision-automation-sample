*** Settings ***
Library  SeleniumLibrary

Resource  ${EXECDIR}${/}..${/}..${/}resources${/}common.robot
Resource  ${EXECDIR}${/}..${/}..${/}resources${/}variables.robot


*** Keywords ***
Sign Up Page Is Displayed
    [Documentation]  Verify If Sign Up Page Is Displayed
    Wait Until Page Contains Element  ${SIGN_UP_EMAIL}
    Title Should Be   ${SIGN_UP_TITLE}
