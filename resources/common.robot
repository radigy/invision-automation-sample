*** Settings ***
Library  SeleniumLibrary

Resource  ${EXECDIR}${/}..${/}..${/}resources${/}variables.robot



*** Keywords ***
Setup Browser
    [Documentation]  Sets up used browser
#    Set Selenium Speed    ${SELENIUM_SPEED}
    Run Keyword If  '${COMMON_BROWSER}' == 'Chrome'    Setup Chrome
    Run Keyword If  '${COMMON_BROWSER}' == 'Firefox'   Setup Firefox
    Run Keyword If  '${COMMON_BROWSER}' == 'Chrome-headless'    Setup Chrome    headless

Setup Chrome
    [Documentation]  Sets up the Chrome browser with options for the test session
    [Arguments]    ${mode}=normal
    ${chrome options} =  Evaluate  sys.modules['selenium.webdriver'].ChromeOptions()  sys, selenium.webdriver

    Run Keyword If  '${mode}'=='headless'    Call Method  ${chrome options}  add_argument  headless
    Call Method  ${chrome options}  add_argument  no-sandbox
    Call Method  ${chrome options}  add_argument  incognito
    Create Webdriver  Chrome  chrome_options=${chrome options}
    Maximize Browser Window
    Open Invision Home Page

Open Invision Home Page
    [Documentation]  Open Invision web url
    Go to  ${HOME_PAGE_URL}
    Wait Until Page Contains Element  ${HOME_PAGE_LOGIN_LINK}
    Accept Cookies

Accept Cookies
    [Documentation]  Accept Cookies Popup
    Wait Until Page Contains Element  ${HOME_PAGE_COOKIES_MODAL}
    Wait Until Element Is Visible  ${HOME_PAGE_COOKIES_ACCEPT_BUTTON}
    Click Button  ${HOME_PAGE_COOKIES_ACCEPT_BUTTON}