*** Variables ***

#home page section
${HOME_PAGE_URL}  https://www.invisionapp.com/
${HOMR_PAGE_TITLE}  InVision | Digital product design, workflow & collaboration
${HOME_PAGE_LOGIN_LINK}  xpath://a[contains(text(),'Sign in')]    #TODO fragile, improve xpath or change locator
${HOME_PAGE_LOGIN_BUTTON}  https://projects.invisionapp.com/d/login
${HOME_PAGE_COOKIES_MODAL}  id:truste-consent-content
${HOME_PAGE_COOKIES_ACCEPT_BUTTON}  id:truste-consent-button

#login section
${LOGIN_PAGE_TITLE}  InVision - Sign In
${LOGIN_MAIL_INPUT}  id:signin_email
${LOGIN_PASSWORD_INPUT}  id:signin_password
${LOGIN_SING_IN_BUTTON}  xpath://span[text()='Sign in']    #TODO fragile, improve xpath or change locator
${LOGIN_VALIDATION_ERROR_MESSAGE}    You entered an incorrect email, password, or both.
${LOGIN_GET_STARTED_BUTTON}  xpath://span[text()='Get started']    #TODO fragile, improve xpath or change locator

#sign up section
${SIGN_UP_TITLE}  Sign Up
${SIGN_UP_EMAIL}  id:email

