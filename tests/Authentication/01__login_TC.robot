*** Settings ***
Library  SeleniumLibrary

Test Setup  Setup Browser
Test Teardown    Close Browser

Resource  ${EXECDIR}${/}..${/}..${/}resources${/}common.robot
Resource  ${EXECDIR}${/}..${/}..${/}resources${/}variables.robot
Resource  ${EXECDIR}${/}..${/}..${/}resources${/}PO${/}login.robot
Resource  ${EXECDIR}${/}..${/}..${/}resources${/}PO${/}signUp.robot


*** Variables ***
${email}  wrong@mail8765123.com
${password}  wrongpass


*** Test Cases ***
Login With Invalid Credentials
    [Documentation]  Login with invalid e-mail address and invalid password. Should trigger validation error message
    [Tags]  smoke  login
    Given User Navigate To Login Page
    When Fill Email And Password With Invalid Credentials  ${email}  ${password}
    And Click Sign In Button
    Then Login Validation Error Message Is Displayed

Verify If Get Started Button Navigate From Login Section To Sign Up Section
    [Documentation]  Verify If Get Started Button Navigate From Login Section To Sign Up Section
    [Tags]  smoke  login
    Given User Navigate To Login Page
    When Click Get Started Button
    Then Sign Up Page Is Displayed

